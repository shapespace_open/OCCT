// Created on: 2016-04-19
// Copyright (c) 2016 OPEN CASCADE SAS
// Created by: Oleg AGASHIN
//
// This file is part of Open CASCADE Technology software library.
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Lesser General Public License version 2.1 as published
// by the Free Software Foundation, with special exception defined in the file
// OCCT_LGPL_EXCEPTION.txt. Consult the file LICENSE_LGPL_21.txt included in OCCT
// distribution for complete text of the license and disclaimer of any warranty.
//
// Alternatively, this file may be used under the terms of Open CASCADE
// commercial license or contractual agreement.

#include <BRepMesh_EdgeDiscret.hxx>
#include <BRepMesh_Deflection.hxx>
#include <IMeshData_Model.hxx>
#include <IMeshData_Face.hxx>
#include <IMeshData_PCurve.hxx>
#include <TopExp.hxx>
#include <BRepMesh_ShapeTool.hxx>
#include <BRepMesh_EdgeTessellationExtractor.hxx>
#include <IMeshData_ParametersListArrayAdaptor.hxx>
#include <BRepMesh_CurveTessellator.hxx>
#include <OSD_Parallel.hxx>
#include <cmath>

IMPLEMENT_STANDARD_RTTIEXT(BRepMesh_EdgeDiscret, IMeshTools_ModelAlgo)

//=======================================================================
// Function: Constructor
// Purpose : 
//=======================================================================
BRepMesh_EdgeDiscret::BRepMesh_EdgeDiscret ()
{
}

//=======================================================================
// Function: Destructor 
// Purpose : 
//=======================================================================
BRepMesh_EdgeDiscret::~BRepMesh_EdgeDiscret ()
{
}

//=======================================================================
// Function: CreateFreeEdgeTessellator
// Purpose : 
//=======================================================================
Handle(IMeshTools_CurveTessellator) BRepMesh_EdgeDiscret::CreateEdgeTessellator(
  const IMeshData::IEdgeHandle& theDEdge,
  const IMeshTools_Parameters&  theParameters,
  const Standard_Integer        theMinPointsNb)
{
  return new BRepMesh_CurveTessellator(theDEdge, theParameters, theMinPointsNb);
}

//=======================================================================
// Function: CreateEdgeTessellator
// Purpose : 
//=======================================================================
Handle(IMeshTools_CurveTessellator) BRepMesh_EdgeDiscret::CreateEdgeTessellator(
  const IMeshData::IEdgeHandle& theDEdge,
  const TopAbs_Orientation      theOrientation,
  const IMeshData::IFaceHandle& theDFace,
  const IMeshTools_Parameters&  theParameters,
  const Standard_Integer        theMinPointsNb)
{
  return theDEdge->GetSameParam() ? 
    new BRepMesh_CurveTessellator(theDEdge, theParameters, theMinPointsNb) :
    new BRepMesh_CurveTessellator(theDEdge, theOrientation, theDFace, theParameters, theMinPointsNb);
}

//=======================================================================
// Function: CreateEdgeTessellationExtractor
// Purpose : 
//=======================================================================
Handle(IMeshTools_CurveTessellator) BRepMesh_EdgeDiscret::CreateEdgeTessellationExtractor(
  const IMeshData::IEdgeHandle& theDEdge,
  const IMeshData::IFaceHandle& theDFace)
{
  return new BRepMesh_EdgeTessellationExtractor(theDEdge, theDFace);
}

//=======================================================================
// Function: Perform
// Purpose : 
//=======================================================================
Standard_Boolean BRepMesh_EdgeDiscret::performInternal (
  const Handle (IMeshData_Model)& theModel,
  const IMeshTools_Parameters&    theParameters,
  const Message_ProgressRange&    theRange)
{
  (void )theRange;
  myModel      = theModel;
  myParameters = theParameters;

  if (myModel.IsNull())
  {
    return Standard_False;
  }

  OSD_Parallel::For (0, myModel->EdgesNb (), *this, !myParameters.InParallel);

  myModel.Nullify(); // Do not hold link to model.
  return Standard_True;
}

//=======================================================================
// Function: process
// Purpose : 
//=======================================================================
void BRepMesh_EdgeDiscret::process (const Standard_Integer theEdgeIndex) const
{
  const IMeshData::IEdgeHandle& aDEdge = myModel->GetEdge (theEdgeIndex);
  try
  {
    OCC_CATCH_SIGNALS

    BRepMesh_Deflection::ComputeDeflection (aDEdge, myModel->GetMaxSize (), myParameters);
  
    Handle (IMeshTools_CurveTessellator) aEdgeTessellator;
    if (!aDEdge->IsFree ())
    {
      // Iterate over pcurves and check deflection on corresponding face.
      Standard_Real    aMinDeflection = RealLast ();
      Standard_Integer aMinPCurveIndex = -1;
      for (Standard_Integer aPCurveIt = 0; aPCurveIt < aDEdge->PCurvesNb (); ++aPCurveIt)
      {
        const IMeshData::IPCurveHandle& aPCurve = aDEdge->GetPCurve (aPCurveIt);
        const Standard_Real aTmpDeflection = checkExistingPolygonAndUpdateStatus(aDEdge, aPCurve);
        if (aTmpDeflection < aMinDeflection)
        {
          // Identify pcurve with the smallest deflection in order to
          // retrieve polygon that represents the most smooth discretization.
          aMinDeflection  = aTmpDeflection;
          aMinPCurveIndex = aPCurveIt;
        }
  
        BRepMesh_ShapeTool::CheckAndUpdateFlags (aDEdge, aPCurve);
      }
  
      if (aMinPCurveIndex != -1)
      {
        aDEdge->SetDeflection (aMinDeflection);
        const IMeshData::IFaceHandle aDFace = aDEdge->GetPCurve(aMinPCurveIndex)->GetFace();
        aEdgeTessellator = CreateEdgeTessellationExtractor(aDEdge, aDFace);
      }
      else
      {
        const IMeshData::IPCurveHandle& aPCurve = aDEdge->GetPCurve(0);
        const IMeshData::IFaceHandle    aDFace  = aPCurve->GetFace();
        aEdgeTessellator = BRepMesh_EdgeDiscret::CreateEdgeTessellator(
          aDEdge, aPCurve->GetOrientation(), aDFace, myParameters);
      }
    }
    else
    {
      TopLoc_Location aLoc;
      const Handle (Poly_Polygon3D)& aPoly3D = BRep_Tool::Polygon3D (aDEdge->GetEdge (), aLoc);
      if (!aPoly3D.IsNull ())
      {
        if (aPoly3D->HasParameters() &&
            BRepMesh_Deflection::IsConsistent (aPoly3D->Deflection(),
                                               aDEdge->GetDeflection(),
                                               myParameters.AllowQualityDecrease))
        {
          // Edge already has suitable 3d polygon.
          aDEdge->SetStatus(IMeshData_Reused);
          return;
        }
        else
        {
          aDEdge->SetStatus(IMeshData_Outdated);
        }
      }
  
      aEdgeTessellator = CreateEdgeTessellator(aDEdge, myParameters);
    }
  
    Tessellate3d (aDEdge, aEdgeTessellator, Standard_True);
    if (!aDEdge->IsFree())
    {
      Tessellate2d(aDEdge, Standard_True);
    }
  }
  catch (Standard_Failure const&)
  {
    aDEdge->SetStatus (IMeshData_Failure);
  }
}

//=======================================================================
// Function: checkExistingPolygonAndUpdateStatus
// Purpose : 
//=======================================================================
Standard_Real BRepMesh_EdgeDiscret::checkExistingPolygonAndUpdateStatus(
  const IMeshData::IEdgeHandle&   theDEdge,
  const IMeshData::IPCurveHandle& thePCurve) const
{
  const TopoDS_Edge& aEdge = theDEdge->GetEdge ();
  const TopoDS_Face& aFace = thePCurve->GetFace ()->GetFace ();

  TopLoc_Location aLoc;
  const Handle (Poly_Triangulation)& aFaceTriangulation =
    BRep_Tool::Triangulation (aFace, aLoc);

  Standard_Real aDeflection = RealLast ();
  if (aFaceTriangulation.IsNull())
  {
    return aDeflection;
  }

  const Handle (Poly_PolygonOnTriangulation)& aPolygon =
    BRep_Tool::PolygonOnTriangulation (aEdge, aFaceTriangulation, aLoc);

  if (!aPolygon.IsNull ())
  {
    Standard_Boolean isConsistent = aPolygon->HasParameters() &&
      BRepMesh_Deflection::IsConsistent (aPolygon->Deflection(),
                                         theDEdge->GetDeflection(),
                                         myParameters.AllowQualityDecrease);

    if (!isConsistent)
    {
      // Nullify edge data and mark discrete pcurve to 
      // notify necessity to mesh the entire face.
      theDEdge->SetStatus(IMeshData_Outdated);
    }
    else
    {
      aDeflection = aPolygon->Deflection();
    }
  }

  return aDeflection;
}

//=======================================================================
// Function: Tessellate3d
// Purpose : 
//=======================================================================
void BRepMesh_EdgeDiscret::Tessellate3d(
  const IMeshData::IEdgeHandle&               theDEdge,
  const Handle (IMeshTools_CurveTessellator)& theTessellator,
  const Standard_Boolean                      theUpdateEnds)
{
  // Create 3d polygon.
  const IMeshData::ICurveHandle& aCurve = theDEdge->GetCurve();

  const TopoDS_Edge& aEdge = theDEdge->GetEdge();
  TopoDS_Vertex aFirstVertex, aLastVertex;
  TopExp::Vertices(aEdge, aFirstVertex, aLastVertex);

  if(aFirstVertex.IsNull() || aLastVertex.IsNull())
    return;

  if (theUpdateEnds)
  {
    gp_Pnt aPoint;
    Standard_Real aParam;
    theTessellator->Value(1, aPoint, aParam);
    aCurve->AddPoint(BRep_Tool::Pnt(aFirstVertex), aParam);
  }

  if (!theDEdge->GetDegenerated())
  {
    for (Standard_Integer i = 2; i < theTessellator->PointsNb(); ++i)
    {
      gp_Pnt aPoint;
      Standard_Real aParam;
      if (!theTessellator->Value(i, aPoint, aParam))
        continue;

      if (theUpdateEnds)
      {
        aCurve->AddPoint(aPoint, aParam);
      }
      else
      {
        aCurve->InsertPoint(aCurve->ParametersNb() - 1, aPoint, aParam);
      }
    }
  }

  if (theUpdateEnds)
  {
    gp_Pnt aPoint;
    Standard_Real aParam;
    theTessellator->Value(theTessellator->PointsNb(), aPoint, aParam);
    aCurve->AddPoint(BRep_Tool::Pnt(aLastVertex), aParam);
  }
}


//=======================================================================
// Function: Tessellate2d
// Purpose : 
//=======================================================================
void BRepMesh_EdgeDiscret::Tessellate2d(
  const IMeshData::IEdgeHandle& theDEdge,
  const Standard_Boolean        theUpdateEnds)
{
  const IMeshData::ICurveHandle& aCurve = theDEdge->GetCurve();
  for (Standard_Integer aPCurveIt = 0; aPCurveIt < theDEdge->PCurvesNb(); ++aPCurveIt)
  {
    const IMeshData::IPCurveHandle& aPCurve = theDEdge->GetPCurve(aPCurveIt);
    const IMeshData::IFaceHandle    aDFace  = aPCurve->GetFace();
    IMeshData::ICurveArrayAdaptorHandle aCurveArray(new IMeshData::ICurveArrayAdaptor(aCurve));
    BRepMesh_EdgeParameterProvider<IMeshData::ICurveArrayAdaptorHandle> aProvider(
      theDEdge, aPCurve->GetOrientation(), aDFace, aCurveArray);

    const Handle(Adaptor2d_Curve2d)& aGeomPCurve = aProvider.GetPCurve();

    Standard_Integer aParamIdx, aParamNb;
    if (theUpdateEnds)
    {
      aParamIdx = 0;
      aParamNb  = aCurve->ParametersNb();
    }
    else
    {
      aParamIdx = 1;
      aParamNb  = aCurve->ParametersNb() - 1;
    }

    for (; aParamIdx < aParamNb; ++aParamIdx)
    {
      const Standard_Real aParam = aProvider.Parameter(aParamIdx, aCurve->GetPoint(aParamIdx));

      gp_Pnt2d aPoint2d;
      aGeomPCurve->D0(aParam, aPoint2d);

      if (theUpdateEnds)
      {
        aPCurve->AddPoint(aPoint2d, aParam);
      }
      else
      {
        aPCurve->InsertPoint(aPCurve->ParametersNb() - 1, aPoint2d, aParam);
      }
    }
  }
}


//=======================================================================
// Function: TessellateEdgeMatch
// Purpose : 
//=======================================================================
Standard_Boolean BRepMesh_EdgeDiscret::TessellateEdgeMatch(
	const IMeshData::IEdgeHandle&               theDEdge,
	const IMeshData::IEdgeHandle&				theDEdgeOpp,
	const Handle(BRepAdaptor_Surface)&			theSurface)
{
	const IMeshData::IPCurveHandle& aPCurveFace = theDEdge->GetPCurve(0);
	const IMeshData::IPCurveHandle& aPCurveFaceOpp = theDEdgeOpp->GetPCurve(0);
	const TopAbs_Orientation aOrientation = aPCurveFace->GetOrientation();
	const TopAbs_Orientation aOrientationOpp = aPCurveFaceOpp->GetOrientation();
	const IMeshData::ICurveHandle& aCurve = theDEdge->GetCurve();
	const Standard_Real aPrecision = Precision::Confusion() * 1e4;

	std::pair<Standard_Real, Standard_Real> aDiffUV;
	std::pair<Standard_Real, Standard_Real> aDiffUVOpp;
	gp_Pnt2d aStartUV, aEndUV, aStartUVOpp, aEndUVOpp;

	std::pair<Standard_Real, Standard_Real> aMeanUV;
	std::pair<Standard_Real, Standard_Real> aMeanUVOpp;

	std::pair<Standard_Real, Standard_Real> aStandardDeviationUV;
	std::pair<Standard_Real, Standard_Real> aStandardDeviationUVOpp;

	Standard_Real sumU = 0.0;
	Standard_Real sumV = 0.0;
	Standard_Integer aParamIdx = 0;
	Standard_Integer aParamNb = aPCurveFace->ParametersNb();
	for (; aParamIdx < aParamNb; ++aParamIdx) {
		sumU += aPCurveFace->GetPoint(aParamIdx).X();
		sumV += aPCurveFace->GetPoint(aParamIdx).Y();
	}

	aMeanUV.first = sumU / aPCurveFace->ParametersNb();
	aMeanUV.second = sumV / aPCurveFace->ParametersNb();


	sumU = 0.0;
	sumV = 0.0;
	aParamIdx = 0;
	aParamNb = aPCurveFaceOpp->ParametersNb();
	for (; aParamIdx < aParamNb; ++aParamIdx) {
		sumU += aPCurveFaceOpp->GetPoint(aParamIdx).X();
		sumV += aPCurveFaceOpp->GetPoint(aParamIdx).Y();
	}

	aMeanUVOpp.first = sumU / aPCurveFaceOpp->ParametersNb();
	aMeanUVOpp.second = sumV / aPCurveFaceOpp->ParametersNb();


	Standard_Real sumSquaredDiffU = 0.0;
	Standard_Real sumSquaredDiffV = 0.0;
	aParamIdx = 0;
	aParamNb = aPCurveFace->ParametersNb();

	for (; aParamIdx < aParamNb; ++aParamIdx) {
		sumSquaredDiffU += (aMeanUV.first - aPCurveFace->GetPoint(aParamIdx).X()) * (aMeanUV.first - aPCurveFace->GetPoint(aParamIdx).X());
		sumSquaredDiffV += (aMeanUV.second - aPCurveFace->GetPoint(aParamIdx).Y()) * (aMeanUV.second - aPCurveFace->GetPoint(aParamIdx).Y());
	}

	aStandardDeviationUV.first = std::sqrt(sumSquaredDiffU / aPCurveFace->ParametersNb());
	aStandardDeviationUV.second = std::sqrt(sumSquaredDiffV / aPCurveFace->ParametersNb());


	sumSquaredDiffU = 0.0;
	sumSquaredDiffV = 0.0;
	aParamIdx = 0;
	aParamNb = aPCurveFaceOpp->ParametersNb();

	for (; aParamIdx < aParamNb; ++aParamIdx) {
		sumSquaredDiffU += (aMeanUVOpp.first - aPCurveFaceOpp->GetPoint(aParamIdx).X()) * (aMeanUVOpp.first - aPCurveFaceOpp->GetPoint(aParamIdx).X());
		sumSquaredDiffV += (aMeanUVOpp.second - aPCurveFaceOpp->GetPoint(aParamIdx).Y()) * (aMeanUVOpp.second - aPCurveFaceOpp->GetPoint(aParamIdx).Y());
	}

	aStandardDeviationUVOpp.first = std::sqrt(sumSquaredDiffU / aPCurveFaceOpp->ParametersNb());
	aStandardDeviationUVOpp.second = std::sqrt(sumSquaredDiffV / aPCurveFaceOpp->ParametersNb());


	if (aOrientation != aOrientationOpp)
	{
		aStartUV = aPCurveFace->GetPoint(0);
		aEndUV = aPCurveFace->GetPoint(aPCurveFace->ParametersNb() - 1);
		aStartUVOpp = aPCurveFaceOpp->GetPoint(0);
		aEndUVOpp = aPCurveFaceOpp->GetPoint(aPCurveFaceOpp->ParametersNb() - 1);
	}

	else
	{
		aStartUV = aPCurveFace->GetPoint(0);
		aEndUV = aPCurveFace->GetPoint(aPCurveFace->ParametersNb() - 1);
		aStartUVOpp = aPCurveFaceOpp->GetPoint(aPCurveFaceOpp->ParametersNb() - 1);
		aEndUVOpp =  aPCurveFaceOpp->GetPoint(0);
	}


	aDiffUV.first = abs(aEndUV.X() - aStartUV.X());
	aDiffUV.second = abs(aEndUV.Y() - aStartUV.Y());

	aDiffUVOpp.first = abs(aEndUVOpp.X() - aStartUVOpp.X());
	aDiffUVOpp.second = abs(aEndUVOpp.Y() - aStartUVOpp.Y());

	// Delta in Both UV coordinates 
	if ((aDiffUV.first > aPrecision || aDiffUVOpp.first > aPrecision) && (aDiffUV.second > aPrecision || aDiffUVOpp.second > aPrecision)) 
	{
		return Standard_False;
	}

	// Changing in Both UV coordinates
	if ((aStandardDeviationUV.first > aPrecision || aStandardDeviationUVOpp.first > aPrecision) && (aStandardDeviationUV.second > aPrecision || aStandardDeviationUVOpp.second > aPrecision))
	{
		return Standard_False;
	}


	// Small angular distance
	if ((aDiffUV.first < aDiffUVOpp.first * 0.25) && (aDiffUV.second < aDiffUV.first))
	{
		return Standard_False;
	}
	
	if ((aDiffUV.second < aDiffUVOpp.second * 0.25) && (aDiffUV.first < aDiffUV.second))
	{
		return Standard_False;
	}

	const TopoDS_Edge& aEdge = theDEdge->GetEdge();
	TopoDS_Vertex aFirstVertex, aLastVertex;
	TopExp::Vertices(aEdge, aFirstVertex, aLastVertex);

	if (aFirstVertex.IsNull() || aLastVertex.IsNull())
		return Standard_False;

	if (!theDEdge->GetDegenerated()) 
	{
		theDEdge->SetSameParam(Standard_False);
		if (aDiffUV.second < aDiffUV.first) 
		{
			const IMeshData::IFaceHandle    aDFace = aPCurveFace->GetFace();
			IMeshData::ICurveArrayAdaptorHandle aCurveArray(new IMeshData::ICurveArrayAdaptor(aCurve));
			BRepMesh_EdgeParameterProvider<IMeshData::ICurveArrayAdaptorHandle> aProvider(
				theDEdge, aPCurveFace->GetOrientation(), aDFace, aCurveArray);

			const Handle(Adaptor2d_Curve2d)& aGeomPCurve = aProvider.GetPCurve();
			const Handle(Adaptor3d_Surface)& aGeomSurface = aProvider.GetSurface();


			Standard_Integer aParamIdxOrient, aInsertLocation;
			gp_Pnt2d aPoint2d;
			Standard_Real aParam, aCurrentParam;
			Standard_Real aUpperLimit, aLowerLimit;
			Standard_Boolean aIsAscending, aIsAscendingOpp;

			if (aStartUV.X() < aEndUV.X())
			{
				aUpperLimit = aEndUV.X();
				aLowerLimit = aStartUV.X();
				aIsAscending = Standard_True;
			}
			else
			{
				aLowerLimit = aEndUV.X();
				aUpperLimit = aStartUV.X();
				aIsAscending = Standard_False;
			}


			if (aPCurveFaceOpp->GetPoint(0).X() < aPCurveFaceOpp->GetPoint(aPCurveFaceOpp->ParametersNb() - 1).X())
			{
				aIsAscendingOpp = Standard_True;
			}
			else
			{
				aIsAscendingOpp = Standard_False;
			}


			if (abs(aStartUV.X() - aStartUVOpp.X()) < abs(aEndUV.X() - aEndUVOpp.X()))
			{
				aParamIdx = 1;
				aInsertLocation = 1;
				aParamNb = aPCurveFaceOpp->ParametersNb() - 1;

				for (; aParamIdx < aParamNb; ++aParamIdx)
				{
					gp_Pnt aPoint;
					aParamIdxOrient = aIsAscending == aIsAscendingOpp ? aParamIdx : aParamNb - aParamIdx;
					aPoint2d = aParamIdx < aPCurveFace->ParametersNb() - 1 ? aPCurveFace->GetPoint(aParamIdx) : aEndUV;

					if (aPCurveFaceOpp->GetPoint(aParamIdxOrient).X() < aLowerLimit || aPCurveFaceOpp->GetPoint(aParamIdxOrient).X() > aUpperLimit)
					{
						continue;
					}

					aPoint2d = aParamIdx < aPCurveFace->ParametersNb() - 1 ? aPCurveFace->GetPoint(aInsertLocation) : aEndUV;
					aGeomSurface->D0(aPCurveFaceOpp->GetPoint(aParamIdxOrient).X(), aPoint2d.Y(), aPoint);

					const gp_Trsf& aTrsf = theSurface->Trsf();
					aPoint.Transform(aTrsf);
					
					aParam = aProvider.Parameter(aInsertLocation, aPoint);

					aCurrentParam = aPCurveFace->GetParameter(aInsertLocation);
					
					if (aCurrentParam > 0) {
						if (abs((aParam - aCurrentParam) / aCurrentParam) > 0.5)
						{
							continue;
						}
					}

					aGeomPCurve->D0(aParam, aPoint2d);

					if (aInsertLocation < aPCurveFace->ParametersNb() - 1)
					{
						aPCurveFace->RemovePoint(aInsertLocation);
						aPCurveFace->InsertPoint(aInsertLocation, aPoint2d, aParam);
						++aInsertLocation;
					}
					else
					{
						aPCurveFace->InsertPoint(aPCurveFace->ParametersNb() - 1, aPoint2d, aParam);
						++aInsertLocation;
					}
				}
			}
			else
			{
				aParamIdx = aPCurveFaceOpp->ParametersNb() - 2;
				aInsertLocation = aPCurveFace->ParametersNb() - 2;
				aParamNb = aPCurveFaceOpp->ParametersNb() - 1;
				

				for (; aParamIdx > 0; --aParamIdx)
				{
					aParamIdxOrient = aIsAscending == aIsAscendingOpp ? aParamIdx : aParamNb - aParamIdx;
					if (aPCurveFaceOpp->GetPoint(aParamIdxOrient).X() < aLowerLimit || aPCurveFaceOpp->GetPoint(aParamIdxOrient).X() > aUpperLimit)
					{
						continue;
					}

					gp_Pnt aPoint;

					aPoint2d = aParamIdx < aPCurveFace->ParametersNb() - 1 ? aPCurveFace->GetPoint(aInsertLocation) : aEndUV;
					aGeomSurface->D0(aPCurveFaceOpp->GetPoint(aParamIdxOrient).X(), aPoint2d.Y(), aPoint);

					const gp_Trsf& aTrsf = theSurface->Trsf();
					aPoint.Transform(aTrsf);

					aParam = aProvider.Parameter(aInsertLocation, aPoint);

					aCurrentParam = aPCurveFace->GetParameter(aInsertLocation);

					if (aCurrentParam > 0) {
						if (abs((aParam - aCurrentParam) / aCurrentParam) > 0.5)
						{
							continue;
						}
					}

					aGeomPCurve->D0(aParam, aPoint2d);

					if (aInsertLocation >= 1)
					{
						aPCurveFace->RemovePoint(aInsertLocation);
						aPCurveFace->InsertPoint(aInsertLocation, aPoint2d, aParam);
						--aInsertLocation;
					}
					else
					{
						aPCurveFace->InsertPoint(1, aPoint2d, aParam);
					}
				}
			}


			for (Standard_Integer i = 1; i < aPCurveFace->ParametersNb() - 1; ++i)
			{
				if (aPCurveFace->GetPoint(i).X() < aPCurveFace->GetPoint(i - 1).X() && aIsAscending) 
				{
					for (Standard_Integer aPCurveIt = 0; aPCurveIt < theDEdge->PCurvesNb(); ++aPCurveIt)
					{
						const IMeshData::IPCurveHandle& aPCurve = theDEdge->GetPCurve(aPCurveIt);
						aPCurve->RemovePoint(i);
					}
					aCurve->RemovePoint(i);
				}

				if (aPCurveFace->GetPoint(i).X() < aPCurveFace->GetPoint(i + 1).X() && !aIsAscending)
				{
					for (Standard_Integer aPCurveIt = 0; aPCurveIt < theDEdge->PCurvesNb(); ++aPCurveIt)
					{
						const IMeshData::IPCurveHandle& aPCurve = theDEdge->GetPCurve(aPCurveIt);
						aPCurve->RemovePoint(i);
					}
					aCurve->RemovePoint(i);
				}
			}
		}
		else
		{
			const IMeshData::IFaceHandle    aDFace = aPCurveFace->GetFace();
			IMeshData::ICurveArrayAdaptorHandle aCurveArray(new IMeshData::ICurveArrayAdaptor(aCurve));
			BRepMesh_EdgeParameterProvider<IMeshData::ICurveArrayAdaptorHandle> aProvider(
				theDEdge, aPCurveFace->GetOrientation(), aDFace, aCurveArray);

			const Handle(Adaptor2d_Curve2d)& aGeomPCurve = aProvider.GetPCurve();
			const Handle(Adaptor3d_Surface)& aGeomSurface = aProvider.GetSurface();
			Standard_Real aUpperLimit, aLowerLimit;
			Standard_Boolean aIsAscending, aIsAscendingOpp;
			Standard_Real aParam, aCurrentParam;

			if (aStartUV.Y() < aEndUV.Y())
			{
				aUpperLimit = aEndUV.Y();
				aLowerLimit = aStartUV.Y();
				aIsAscending = Standard_True;
			}
			else
			{
				aLowerLimit = aEndUV.Y();
				aUpperLimit = aStartUV.Y();
				aIsAscending = Standard_False;
			}

			if (aPCurveFaceOpp->GetPoint(0).Y() < aPCurveFaceOpp->GetPoint(aPCurveFaceOpp->ParametersNb() - 1).Y())
			{
				aIsAscendingOpp = Standard_True;
			}
			else
			{
				aIsAscendingOpp = Standard_False;
			}



			if (abs(aStartUV.Y() - aStartUVOpp.Y()) < abs(aEndUV.Y() - aEndUVOpp.Y()))
			{
				Standard_Integer aParamIdxOrient, aInsertLocation;
				aParamIdx = 1;
				aParamNb = aPCurveFaceOpp->ParametersNb() - 1;
				aInsertLocation = 1;
				gp_Pnt2d aPoint2d;

				for (; aParamIdx < aParamNb; ++aParamIdx)
				{
					aParamIdxOrient = aIsAscending == aIsAscendingOpp ? aParamIdx : aParamNb - aParamIdx;
					if (aPCurveFaceOpp->GetPoint(aParamIdxOrient).Y() < aLowerLimit || aPCurveFaceOpp->GetPoint(aParamIdxOrient).Y() > aUpperLimit)
					{
						continue;
					}

					gp_Pnt aPoint;
					aPoint2d = aParamIdx < aPCurveFace->ParametersNb() - 1 ? aPCurveFace->GetPoint(aInsertLocation) : aEndUV;
					aGeomSurface->D0(aPoint2d.X(), aPCurveFaceOpp->GetPoint(aParamIdxOrient).Y(), aPoint);

					const gp_Trsf& aTrsf = theSurface->Trsf();
					aPoint.Transform(aTrsf);

					aParam = aProvider.Parameter(aInsertLocation, aPoint); 
					aCurrentParam = aPCurveFace->GetParameter(aInsertLocation);

					if (aCurrentParam > 0) {
						if (abs((aParam - aCurrentParam) / aCurrentParam) > 0.5)
						{
							continue;
						}
					}

					aGeomPCurve->D0(aParam, aPoint2d);
					if (aParamIdx < aPCurveFace->ParametersNb() - 1)
					{
						aPCurveFace->RemovePoint(aInsertLocation);
						aPCurveFace->InsertPoint(aInsertLocation, aPoint2d, aParam);
						++aInsertLocation;
					}
					else
					{
						aPCurveFace->InsertPoint(aPCurveFace->ParametersNb() - 1, aPoint2d, aParam);
						++aInsertLocation;
					}
				}
			}
			else
			{
				Standard_Integer aParamIdxOrient, aInsertLocation;
				aParamIdx = aPCurveFaceOpp->ParametersNb() - 2;
				aInsertLocation = aPCurveFace->ParametersNb() - 2;
				aParamNb = aPCurveFaceOpp->ParametersNb() - 1;
				gp_Pnt2d aPoint2d;

				for (; aParamIdx > 0; --aParamIdx)
				{
					gp_Pnt aPoint;
					aParamIdxOrient = aIsAscending == aIsAscendingOpp ? aParamIdx : aParamNb - aParamIdx;

					if (aPCurveFaceOpp->GetPoint(aParamIdxOrient).Y() < aLowerLimit || aPCurveFaceOpp->GetPoint(aParamIdxOrient).Y() > aUpperLimit)
					{
						continue;
					}

					aPoint2d = aParamIdx < aPCurveFace->ParametersNb() - 1 ? aPCurveFace->GetPoint(aInsertLocation) : aEndUV;
					aGeomSurface->D0(aPoint2d.X(), aPCurveFaceOpp->GetPoint(aParamIdxOrient).Y(), aPoint);

					const gp_Trsf& aTrsf = theSurface->Trsf();
					aPoint.Transform(aTrsf);

					aParam = aProvider.Parameter(aInsertLocation, aPoint);

					aCurrentParam = aPCurveFace->GetParameter(aInsertLocation);

					if (aCurrentParam > 0) {
						if (abs((aParam - aCurrentParam) / aCurrentParam) > 0.5)
						{
							continue;
						}
					}

					aGeomPCurve->D0(aParam, aPoint2d);
					if (aInsertLocation >= 1)
					{
						aPCurveFace->RemovePoint(aInsertLocation);
						aPCurveFace->InsertPoint(aInsertLocation, aPoint2d, aParam);
						--aInsertLocation;
					}
					else
					{
						aPCurveFace->InsertPoint(1, aPoint2d, aParam);
					}
				}
			}
			for (Standard_Integer i = 1; i < aPCurveFace->ParametersNb() - 1; ++i)
			{
				if (aPCurveFace->GetPoint(i).Y() < aPCurveFace->GetPoint(i - 1).Y() && aIsAscending)
				{
					for (Standard_Integer aPCurveIt = 0; aPCurveIt < theDEdge->PCurvesNb(); ++aPCurveIt)
					{
						const IMeshData::IPCurveHandle& aPCurve = theDEdge->GetPCurve(aPCurveIt);
						aPCurve->RemovePoint(i);
					}
					aCurve->RemovePoint(i);
				}

				if (aPCurveFace->GetPoint(i).Y() < aPCurveFace->GetPoint(i + 1).Y() && !aIsAscending)
				{
					for (Standard_Integer aPCurveIt = 0; aPCurveIt < theDEdge->PCurvesNb(); ++aPCurveIt)
					{
						const IMeshData::IPCurveHandle& aPCurve = theDEdge->GetPCurve(aPCurveIt);
						aPCurve->RemovePoint(i);
					}
					aCurve->RemovePoint(i);
				}
			}

		}


		for (Standard_Integer aPCurveIt = 1; aPCurveIt < theDEdge->PCurvesNb(); ++aPCurveIt)
		{
			const IMeshData::IPCurveHandle& aPCurve = theDEdge->GetPCurve(aPCurveIt);
			const IMeshData::IFaceHandle    aDFace = aPCurve->GetFace();
			IMeshData::ICurveArrayAdaptorHandle aCurveArray(new IMeshData::ICurveArrayAdaptor(aCurve));
			BRepMesh_EdgeParameterProvider<IMeshData::ICurveArrayAdaptorHandle> aProvider(
				theDEdge, aPCurve->GetOrientation(), aDFace, aCurveArray);

			const Handle(Adaptor2d_Curve2d)& aGeomPCurve = aProvider.GetPCurve();

			aParamIdx = 1;
			aParamNb = aPCurveFace->ParametersNb() - 1;
			Standard_Real aParam;
			gp_Pnt2d aPoint2d;

			for (; aParamIdx < aParamNb; ++aParamIdx)
			{
				aParam = aPCurveFace->GetParameter(aParamIdx);
				aGeomPCurve->D0(aParam, aPoint2d);
				if (aParamIdx < aPCurve->ParametersNb() - 1)
				{
					aPCurve->RemovePoint(aParamIdx);
					aPCurve->InsertPoint(aParamIdx, aPoint2d, aParam);
				}
				else
				{
					aPCurve->InsertPoint(aPCurve->ParametersNb() - 1, aPoint2d, aParam);
				}
			}
		}
		const BRepAdaptor_Curve aAdaptCurve(aEdge);
		for (Standard_Integer i = 1; i < aPCurveFace->ParametersNb() - 1; ++i)
		{
			Standard_Real aParam = aPCurveFace->GetParameter(i);
			gp_Pnt aPoint;
			aAdaptCurve.D0(aParam, aPoint);
			if (i < aCurve->ParametersNb() - 1)
			{
				aCurve->RemovePoint(i);
				aCurve->InsertPoint(i, aPoint, aParam);
			}
			else
			{
				aCurve->InsertPoint(aCurve->ParametersNb() - 1, aPoint, aParam);
			}
		}
	}
	return Standard_True;
}