// Created on: 2016-04-19
// Copyright (c) 2016 OPEN CASCADE SAS
// Created by: Oleg AGASHIN
//
// This file is part of Open CASCADE Technology software library.
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Lesser General Public License version 2.1 as published
// by the Free Software Foundation, with special exception defined in the file
// OCCT_LGPL_EXCEPTION.txt. Consult the file LICENSE_LGPL_21.txt included in OCCT
// distribution for complete text of the license and disclaimer of any warranty.
//
// Alternatively, this file may be used under the terms of Open CASCADE
// commercial license or contractual agreement.

#include <BRepMesh_NURBSRangeSplitter.hxx>
#include <algorithm>
#include <BRepMesh_GeomTool.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <GeomLib.hxx>
#include <IMeshData_Edge.hxx>
#include <IMeshData_Wire.hxx>
#include <NCollection_Handle.hxx>
#include <vector>
#include <map>


namespace
{
  class AnalyticalFilter
  {
  public:
    //! Constructor.
    AnalyticalFilter(
      const IMeshData::IFaceHandle&             theDFace,
      const GeomAbs_IsoType                     theIsoType,
      const Handle(IMeshData::SequenceOfReal)&  theParams,
      const Handle(IMeshData::SequenceOfReal)&  theControlParams,
      const Handle(IMeshData::MapOfReal)&       theParamsForbiddenToRemove,
      const Handle(IMeshData::MapOfReal)&       theControlParamsForbiddenToRemove)
      : myDFace(theDFace),
        mySurface(myDFace->GetSurface()->Surface().Surface()),
        myIsoU(theIsoType == GeomAbs_IsoU),
        myParams(theParams),
        myControlParams(theControlParams),
        myParamsForbiddenToRemove(theParamsForbiddenToRemove),
        myControlParamsForbiddenToRemove(theControlParamsForbiddenToRemove),
        myAllocator(new NCollection_IncAllocator(IMeshData::MEMORY_BLOCK_SIZE_HUGE)),
        myControlParamsToRemove(new IMeshData::MapOfReal(1, myAllocator)),
        myCurrParam(0.0),
        myCurrControlParam(0.0),
        myPrevControlParam(0.0)
    {
    }

    //! Returns map of parameters supposed to be removed.
    const Handle(IMeshData::MapOfReal)& GetControlParametersToRemove(
      const IMeshTools_Parameters& theParameters)
    {
      myParameters = theParameters;

      Standard_Integer aStartIndex, aEndIndex;
      if (myIsoU)
      {
        aStartIndex = 1;
        aEndIndex = myParams->Length();
      }
      else
      {
        aStartIndex = 2;
        aEndIndex = myParams->Length() - 1;
      }

      for (Standard_Integer i = aStartIndex; i <= aEndIndex; ++i)
      {
        myCurrParam = myParams->Value(i);
		myIso = new GeomAdaptor_Curve(myIsoU ? mySurface->UIso(myCurrParam) : mySurface->VIso(myCurrParam));

        myPrevControlParam = myControlParams->Value(1);
        myIso->D1(myPrevControlParam, myPrevControlPnt, myPrevControlVec);
        for (Standard_Integer j = 2; j <= myControlParams->Length();)
        {
          j += checkControlPointAndMoveOn(j);
        }
      }

      return myControlParamsToRemove;
    }

  private:

    //! Checks the given control point for deviation.
    //! Returns number of steps to be used to move point iterator.
    Standard_Integer checkControlPointAndMoveOn(const Standard_Integer theIndex)
    {
      Standard_Integer aMoveSteps = 0;
      myCurrControlParam = myControlParams->Value(theIndex);
      myIso->D1(myCurrControlParam, myCurrControlPnt, myCurrControlVec);

      const Standard_Real aMidParam = 0.5 * (myPrevControlParam + myCurrControlParam);
      const gp_Pnt aMidPnt = myIso->Value(aMidParam);

      const Standard_Real aSqDist = BRepMesh_GeomTool::SquareDeflectionOfSegment(
        myPrevControlPnt, myCurrControlPnt, aMidPnt);

      Standard_Real anAngle = 0.0;
      
      if ((myPrevControlVec.SquareMagnitude() > Precision::SquareConfusion()) &&
          (myCurrControlVec.SquareMagnitude() > Precision::SquareConfusion()))
      {
        anAngle = myPrevControlVec.Angle(myCurrControlVec);
      }

      const Standard_Real aSqMaxDeflection = myDFace->GetDeflection() *
        myDFace->GetDeflection();

      if (((aSqDist > aSqMaxDeflection) || (anAngle > myParameters.AngleInterior)) &&
          aSqDist > myParameters.MinSize * myParameters.MinSize)
      {
        // insertion 
        myControlParams->InsertBefore(theIndex, aMidParam);
      }
      else
      {
        // Here we should leave at least 3 parameters as far as
        // we must have at least one parameter related to surface
        // internals in order to prevent movement of triangle body
        // outside the surface in case of highly curved ones, e.g.
        // BSpline springs.
        if (((aSqDist < aSqMaxDeflection) || (anAngle < myParameters.AngleInterior)) &&
            myControlParams->Length() > 3 && theIndex < myControlParams->Length())
        {
          // Remove too dense points
          const Standard_Real aTmpParam = myControlParams->Value(theIndex + 1);
          if (checkParameterForDeflectionAndUpdateCache(aTmpParam))
          {
            ++aMoveSteps;
          }
        }

        myPrevControlParam = myCurrControlParam;
        myPrevControlPnt   = myCurrControlPnt;
        myPrevControlVec   = myCurrControlVec;

        ++aMoveSteps;
      }

      return aMoveSteps;
    }

    //! Checks whether the given param suits specified deflection. Updates cache.
    Standard_Boolean checkParameterForDeflectionAndUpdateCache(const Standard_Real theParam)
    {
      gp_Pnt aTmpPnt;
      gp_Vec aTmpVec;
      myIso->D1(theParam, aTmpPnt, aTmpVec);

      const Standard_Real aTmpMidParam = 0.5 * (myPrevControlParam + theParam);
      const gp_Pnt        aTmpMidPnt = myIso->Value(aTmpMidParam);

      // Lets check next parameter.
      // If it also fits deflection, we can remove previous parameter.
      const Standard_Real aSqDist = BRepMesh_GeomTool::SquareDeflectionOfSegment(
        myPrevControlPnt, aTmpPnt, aTmpMidPnt);

      if (aSqDist < myDFace->GetDeflection() * myDFace->GetDeflection())
      {
        // Lets check parameters for angular deflection.
        if (myPrevControlVec.SquareMagnitude() < gp::Resolution() ||
            aTmpVec.SquareMagnitude()          < gp::Resolution() ||
            myPrevControlVec.Angle(aTmpVec)    < myParameters.AngleInterior)
        {
          // For current Iso line we can remove this parameter.
          myControlParamsToRemove->Add(myCurrControlParam);
          myCurrControlParam = theParam;
          myCurrControlPnt   = aTmpPnt;
          myCurrControlVec   = aTmpVec;
          return Standard_True;
        }
        else
        {
          // We have found a place on the surface refusing 
          // removement of this parameter.
          myParamsForbiddenToRemove       ->Add(myCurrParam);
          myControlParamsForbiddenToRemove->Add(myCurrControlParam);
        }
      }

      return Standard_False;
    }

  private:

    IMeshData::IFaceHandle                myDFace;
    Handle(Geom_Surface)                  mySurface;
    Standard_Boolean                      myIsoU;
    Handle(IMeshData::SequenceOfReal)     myParams;
    Handle(IMeshData::SequenceOfReal)     myControlParams;

    Handle(IMeshData::MapOfReal)          myParamsForbiddenToRemove;
    Handle(IMeshData::MapOfReal)          myControlParamsForbiddenToRemove;

    Handle(NCollection_IncAllocator)      myAllocator;
    Handle(IMeshData::MapOfReal)          myControlParamsToRemove;


    IMeshTools_Parameters                 myParameters;
    NCollection_Handle<GeomAdaptor_Curve> myIso;

    Standard_Real                         myCurrParam;

    Standard_Real                         myCurrControlParam;
    gp_Pnt                                myCurrControlPnt;
    gp_Vec                                myCurrControlVec;

    Standard_Real                         myPrevControlParam;
    gp_Pnt                                myPrevControlPnt;
    gp_Vec                                myPrevControlVec;
  };

  //! Adds param to map if it fits specified range.
  Standard_Boolean addParam(
    const Standard_Real&                           theParam,
    const std::pair<Standard_Real, Standard_Real>& theRange,
    IMeshData::IMapOfReal&                         theParams)
  {
    if (theParam < theRange.first ||
        theParam > theRange.second)
    {
      return Standard_False;
    }

    theParams.Add(theParam);
    return Standard_True;
  }

  //! Initializes parameters map using CN intervals.
  Standard_Boolean initParamsFromIntervals(
    const TColStd_Array1OfReal&                    theIntervals,
    const std::pair<Standard_Real, Standard_Real>& theRange,
    const Standard_Boolean                         isSplitIntervals,
    IMeshData::IMapOfReal&                         theParams)
  {
    Standard_Boolean isAdded = Standard_False;

    for (Standard_Integer i = theIntervals.Lower(); i <= theIntervals.Upper(); ++i)
    {
      const Standard_Real aStartParam = theIntervals.Value(i);
      if (addParam(aStartParam, theRange, theParams))
      {
        isAdded = Standard_True;
      }

      if (isSplitIntervals && i < theIntervals.Upper())
      {
        const Standard_Real aMidParam = (aStartParam + theIntervals.Value(i + 1)) / 2.;
        if (addParam(aMidParam, theRange, theParams))
        {
          isAdded = Standard_True;
        }
      }
    }

    return isAdded;
  }

  //! Checks whether intervals should be split.
  //! Returns true in case if it is impossible to compute normal 
  //! directly on intervals, false is returned elsewhere.
  Standard_Boolean toSplitIntervals (const Handle (Geom_Surface)&  theSurf,
                                     const TColStd_Array1OfReal  (&theIntervals)[2])
  {
    Standard_Integer aIntervalU = theIntervals[0].Lower ();
    for (; aIntervalU <= theIntervals[0].Upper (); ++aIntervalU)
    {
      const Standard_Real aParamU = theIntervals[0].Value(aIntervalU);
      if (Precision::IsInfinite (aParamU))
        continue;

      Standard_Integer aIntervalV = theIntervals[1].Lower ();
      for (; aIntervalV <= theIntervals[1].Upper (); ++aIntervalV)
      {
        gp_Dir aNorm;
        const Standard_Real aParamV = theIntervals[1].Value(aIntervalV);
        if (Precision::IsInfinite (aParamV))
          continue;

        if (GeomLib::NormEstim (theSurf, gp_Pnt2d (aParamU, aParamV), Precision::Confusion (), aNorm) != 0)
        {
          return Standard_True;
        }
        // TODO: do not split intervals if there is no normal in the middle of interval.
      }
    }

    return Standard_False;
  }
}

//=======================================================================
// Function: AdjustRange
// Purpose : 
//=======================================================================
void BRepMesh_NURBSRangeSplitter::AdjustRange()
{
  BRepMesh_DefaultRangeSplitter::AdjustRange();
  mySurfaceType = GetSurface()->GetType();

  if (mySurfaceType == GeomAbs_BezierSurface)
  {
    const std::pair<Standard_Real, Standard_Real>& aRangeU = GetRangeU();
    const std::pair<Standard_Real, Standard_Real>& aRangeV = GetRangeV();

    myIsValid = !(aRangeU.first  < -0.5 ||
                  aRangeU.second >  1.5 ||
                  aRangeV.first  < -0.5 ||
                  aRangeV.second >  1.5);
  }
}

//=======================================================================
// Function: GenerateSurfaceNodes
// Purpose : 
//=======================================================================
Handle(IMeshData::ListOfPnt2d) BRepMesh_NURBSRangeSplitter::GenerateSurfaceNodes(
	const IMeshTools_Parameters& theParameters) const
{

	if (!initParameters())
	{
		return Handle(IMeshData::ListOfPnt2d)();
	}

	const std::pair<Standard_Real, Standard_Real>& aRangeU = GetRangeU();
	const std::pair<Standard_Real, Standard_Real>& aRangeV = GetRangeV();
	const std::pair<Standard_Real, Standard_Real>& aDelta = GetDelta();

	const Standard_Real                 aDefFace = GetDFace()->GetDeflection();
	const Handle(BRepAdaptor_Surface)& gFace = GetSurface();
	Handle(Geom_Surface)                aSurface = gFace->Surface().Surface();

	const Handle(NCollection_IncAllocator) aTmpAlloc =
		new NCollection_IncAllocator(IMeshData::MEMORY_BLOCK_SIZE_HUGE);


	// BEGIN MATCHING CHANGES

	// Initialise edge and vertex counts
	Standard_Integer aPointCount = 0;
	Standard_Real aEdgePointCount = 0;
	Standard_Real aEdgeIndex = 1;

	// Initialise matrices to hold data points
	std::vector<std::vector<Standard_Real>> aMetaDataPointMatrix;
	std::vector<std::vector<Standard_Real>> aCoordinateMatrix;
	std::vector<std::vector<Standard_Real>> aMatchingMatrix;

	// Make used indices map
	std::map<Standard_Integer, Standard_Boolean> aUsedIndices;

	// Initialise U and V arrays, and a matrix containing both coordinates for each point
	std::vector<Standard_Real> aUArray;
	std::vector<Standard_Real> aVArray;

	// Loop around face to populate the arrays and matrix
	const IMeshData::IFaceHandle& aDFace2 = GetDFace();
	for (Standard_Integer aWireIt = 0; aWireIt < aDFace2->WiresNb(); ++aWireIt)
	{
		const IMeshData::IWireHandle& aDWire2 = aDFace2->GetWire(aWireIt);
		for (Standard_Integer aEdgeIt = 0; aEdgeIt < aDWire2->EdgesNb(); ++aEdgeIt)
		{
			const IMeshData::IEdgePtr& aDEdge2 = aDWire2->GetEdge(aEdgeIt);
			for (Standard_Integer aPCurveIt = 0; aPCurveIt < aDEdge2->PCurvesNb(); ++aPCurveIt)
			{
				const IMeshData::IPCurveHandle& aDPCurve2 = aDEdge2->GetPCurve(aPCurveIt);
				if (aDPCurve2->GetFace() == aDFace2)
				{
					Standard_Real aEdgeMinU = aRangeU.second;
					Standard_Real aEdgeMaxU = aRangeU.first;

					Standard_Real aEdgeMinV = aRangeV.second;
					Standard_Real aEdgeMaxV = aRangeV.first;

					for (Standard_Integer aPointIt = 0; aPointIt < aDPCurve2->ParametersNb(); ++aPointIt)
					{
						const gp_Pnt2d& aPnt2d = aDPCurve2->GetPoint(aPointIt);

						// Populating U and V arrays
						aUArray.push_back(aPnt2d.X());
						aVArray.push_back(aPnt2d.Y());

						aCoordinateMatrix.push_back({ aPnt2d.X(), aPnt2d.Y(), aEdgeIndex });

						if (aPnt2d.X() < aEdgeMinU) 
						{
							aEdgeMinU = aPnt2d.X();
						}

						if (aPnt2d.X() > aEdgeMaxU)
						{
							aEdgeMaxU = aPnt2d.X();
						}

						if (aPnt2d.Y() < aEdgeMinV)
						{
							aEdgeMinV = aPnt2d.Y();
						}

						if (aPnt2d.Y() > aEdgeMaxV) 
						{
							aEdgeMaxV = aPnt2d.Y();
						}

						// Updates counts
						aPointCount++;
						aEdgePointCount++;
					}

					Standard_Real aEdgeRangeU = abs(aEdgeMaxU - aEdgeMinU);
					Standard_Real aEdgeRangeV = abs(aEdgeMaxV - aEdgeMinV);

					aMetaDataPointMatrix.push_back({ aEdgePointCount, aEdgeRangeU, aEdgeRangeV });

					//Updates indices for next edge
					aEdgePointCount = 0;
					aEdgeIndex++;
				}
			}
		}
	}

	// Calculate U and V ranges
	Standard_Real aDeltaRangeU = abs(aRangeU.second - aRangeU.first);
	Standard_Real aDeltaRangeV = abs(aRangeV.second - aRangeV.first);

	// Calculates weight of the NURBS
	Standard_Real aWeight = 0;
	for (Standard_Integer aEdgeIt = 0; aEdgeIt < aMetaDataPointMatrix.size(); aEdgeIt++)
	{
		aWeight += (aMetaDataPointMatrix[aEdgeIt][1] / aDeltaRangeU) * (aMetaDataPointMatrix[aEdgeIt][0] / aPointCount);
	}

	// Decides whether the NURBS is U dominant or not
	Standard_Boolean isUDominant = (aWeight > 0.5) ? true : false;

	auto sortByU = [](const std::vector<Standard_Real>& a, const std::vector<Standard_Real>& b) {
		return a[0] < b[0];
	};

	auto sortByV = [](const std::vector<Standard_Real>& a, const std::vector<Standard_Real>& b) {
		return a[1] < b[1];
	};


	if (isUDominant)
	{
		// Sort by U
		std::sort(aCoordinateMatrix.begin(), aCoordinateMatrix.end(), sortByU);
	}
	else 
	{
		// Sort by V
		std::sort(aCoordinateMatrix.begin(), aCoordinateMatrix.end(), sortByV);
	}

	const Standard_Real filterDistance = theParameters.NURBSMatchingFilterDistance;

	// Matching method
	for (Standard_Integer aPointIt = 0; aPointIt < aPointCount - 1; ++aPointIt)
	{
		if (!(aUsedIndices[aPointIt] || aUsedIndices[aPointIt + 1]) &&
			((aCoordinateMatrix[aPointIt][2] != aCoordinateMatrix[aPointIt + 1][2])))
		{
			Standard_Real diffU = abs(aCoordinateMatrix[aPointIt][0] - aCoordinateMatrix[aPointIt + 1][0]);
			Standard_Real diffV = abs(aCoordinateMatrix[aPointIt][1] - aCoordinateMatrix[aPointIt + 1][1]);

			if ((isUDominant && (abs(aCoordinateMatrix[aPointIt][0] - aRangeU.first) > aDelta.first) &&
				(abs(aCoordinateMatrix[aPointIt + 1][0] - aRangeU.second) > aDelta.first) &&
				(diffV > ((aRangeV.second - aRangeV.first) / 2)) && 
				(diffU < filterDistance * (aDelta.first))) ||
				(!isUDominant && (abs(aCoordinateMatrix[aPointIt][1] - aRangeV.first) > aDelta.second) &&
				(abs(aCoordinateMatrix[aPointIt + 1][1] - aRangeV.second) > aDelta.second) &&
				(diffU > ((aRangeU.second - aRangeU.first) / 2)) &&
				(diffV < filterDistance * (aDelta.second))))
			{
				aMatchingMatrix.push_back(aCoordinateMatrix[aPointIt]);
				aUsedIndices[aPointIt] = true;

				aMatchingMatrix.push_back(aCoordinateMatrix[aPointIt + 1]);
				aUsedIndices[aPointIt + 1] = true;
			}
		}
	}

	// Add points with isoline if they had no matches
	for (Standard_Integer aPointIt = 0; aPointIt < aCoordinateMatrix.size(); aPointIt++)
	{
		if (!aUsedIndices[aPointIt])
		{
			aMatchingMatrix.push_back(aCoordinateMatrix[aPointIt]);

			if (isUDominant) 
			{
				aMatchingMatrix.push_back({ aCoordinateMatrix[aPointIt][0], aRangeV.second - aCoordinateMatrix[aPointIt][1] });
			}
			else 
			{
				aMatchingMatrix.push_back({ aRangeU.second - aCoordinateMatrix[aPointIt][0], aCoordinateMatrix[aPointIt][1] });
			}
		}
	}

	std::vector<std::vector<std::vector<Standard_Real>>> rowPairs;
	for (auto it = aMatchingMatrix.begin(); it != aMatchingMatrix.end(); it += 2) {
		rowPairs.push_back({ *it, *(it + 1) });
	}

	if (isUDominant)
	{
		// Custom comparator to sort based on the first element of the first row in each pair
		auto comparator = [](const std::vector<std::vector<Standard_Real>>& a,
			const std::vector<std::vector<Standard_Real>>& b) {
			return a[0][0] < b[0][0];
		};

		// Sort the pairs based on the first element of the first row in each pair
		std::sort(rowPairs.begin(), rowPairs.end(), comparator);
	}
	else
	{
		// Custom comparator to sort based on the first element of the first row in each pair
		auto comparator = [](const std::vector<std::vector<Standard_Real>>& a,
			const std::vector<std::vector<Standard_Real>>& b) {
			return a[0][1] < b[0][1];
		};

		// Sort the pairs based on the first element of the first row in each pair
		std::sort(rowPairs.begin(), rowPairs.end(), comparator);
	}

	// Reconstruct the sorted matrix
	std::vector<std::vector<Standard_Real>> aPairMatrix;
	for (const auto& pair : rowPairs) {
		aPairMatrix.push_back(pair[0]);
		aPairMatrix.push_back(pair[1]);
	}

	// Sort U and V arrays containing the corresponding coordinate of every point
	std::sort(aUArray.begin(), aUArray.end());
	std::sort(aVArray.begin(), aVArray.end());

	// Filter U coordinates by assigning the same value to two entries that are very close together
	for (Standard_Integer aRow = 1; aRow < aUArray.size(); aRow++)
	{
		if (abs(aUArray[aRow] - aUArray[aRow - 1]) < 0.5 * aDelta.first)
		{
			aUArray[aRow] = aUArray[aRow - 1];
		}
	}

	// Filter V coordinates by assigning the same value to two entries that are very close together
	for (Standard_Integer aRow = 1; aRow < aVArray.size(); aRow++) 
	{
		if (abs(aVArray[aRow] - aVArray[aRow - 1]) < 0.5 * aDelta.second)
		{
			aVArray[aRow] = aVArray[aRow - 1];
		}
	}

	// Push all duplicate values to the end of the array
	auto it = std::unique(aUArray.begin(), aUArray.end());

	// Delete all duplicates
	aUArray.erase(it, aUArray.end());

	// Likewise for V array
	auto it2 = std::unique(aVArray.begin(), aVArray.end());
	aVArray.erase(it2, aVArray.end());

	// Creating map to input to computeGrainAndFilter with U coordinates moved
	IMeshData::IMapOfReal theFilteredUParams;
	for (Standard_Integer aRow = 0; aRow < aUArray.size(); aRow++) 
	{
		theFilteredUParams.Add(aUArray[aRow]);
	}

	// Likewise for V
	IMeshData::IMapOfReal theFilteredVParams;
	for (Standard_Integer aRow = 0; aRow < aVArray.size(); aRow++) 
	{
		theFilteredVParams.Add(aVArray[aRow]);
	}

	// END MATCHING CHANGES


	const Handle(IMeshData::SequenceOfReal) aParams[2] = {
	computeGrainAndFilterParameters(theFilteredUParams, gFace->UResolution(aDefFace),
		aDeltaRangeU, aDelta.first,  theParameters, aTmpAlloc),

	computeGrainAndFilterParameters(theFilteredVParams, gFace->VResolution(aDefFace),
		aDeltaRangeV, aDelta.second, theParameters, aTmpAlloc)
	};

	// insert nodes of the regular grid
	Handle(IMeshData::ListOfPnt2d) aNodes = new IMeshData::ListOfPnt2d(
		new NCollection_IncAllocator(IMeshData::MEMORY_BLOCK_SIZE_HUGE));


	// NEW WAY TO APPEND NODES

	Standard_Boolean toAppendNode;

	if (isUDominant)
	{
		std::vector<std::vector<Standard_Real>> aNodeMatrix;

		for (Standard_Integer aPointIt = 0; aPointIt < aPairMatrix.size() - 1; aPointIt += 2)
		{
			for (Standard_Integer aVParamIt = 1; aVParamIt <= aParams[1]->Length(); ++aVParamIt)
			{
				const Standard_Real aParam2 = aParams[1]->Value(aVParamIt);

				// If the V coordinate of the appended point is very close to the edges, we do not append it
				if (aParam2 < (aRangeV.first + 0.05 * aDeltaRangeV) || aParam2 > (aRangeV.second - 0.05 * aDeltaRangeV))
				{
					continue;
				}

				// Interpolate the value of U coordinate based on the pair of matched points and the V coordinate
				Standard_Real aParam1 = aPairMatrix[aPointIt][0] + ((aPairMatrix[aPointIt + 1][0] - aPairMatrix[aPointIt][0]) /
					(aPairMatrix[aPointIt + 1][1] - aPairMatrix[aPointIt][1])) * (aParam2 - aPairMatrix[aPointIt][1]);

				toAppendNode = true;

				if (!aNodes->IsEmpty())
				{
					for (int l = 0; l < aNodeMatrix.size(); l++)
					{
						Standard_Real aUParamDiff = abs(aNodeMatrix[l][0] - aParam1);
						Standard_Real aVParamDiff = abs(aNodeMatrix[l][1] - aParam2);

						// If the new point is very close to an existent node, we do not add it
						if ((aUParamDiff < aDelta.first) && (aVParamDiff < aDelta.second))
						{
							toAppendNode = false;
							break;
						}
					}
				}

				// Append nodes
				if (toAppendNode) 
				{
					aNodes->Append(gp_Pnt2d(aParam1, aParam2));
					aNodeMatrix.push_back({ aParam1, aParam2 });
				}
			}
		}
	}
	else 
	{
		std::vector<std::vector<Standard_Real>> aNodeMatrix;

		for (Standard_Integer aPointIt = 0; aPointIt < aPairMatrix.size() - 1; aPointIt += 2)
		{
			for (Standard_Integer aUParamIt = 1; aUParamIt <= aParams[0]->Length(); ++aUParamIt)
			{
				const Standard_Real aParam1 = aParams[0]->Value(aUParamIt);

				if (aParam1 < (aRangeU.first + 0.05 * aDeltaRangeU) || aParam1 > (aRangeU.second - 0.05 * aDeltaRangeU))
				{
					continue;
				}

				Standard_Real aParam2 = aPairMatrix[aPointIt][1] + ((aPairMatrix[aPointIt + 1][1] - aPairMatrix[aPointIt][1]) /
					(aPairMatrix[aPointIt + 1][0] - aPairMatrix[aPointIt][0])) * (aParam1 - aPairMatrix[aPointIt][0]);

				toAppendNode = true;

				if (!aNodes->IsEmpty())
				{
					for (int l = 0; l < aNodeMatrix.size(); l++)
					{
						Standard_Real aUParamDiff = abs(aNodeMatrix[l][0] - aParam1);
						Standard_Real aVParamDiff = abs(aNodeMatrix[l][1] - aParam2);

						if ((aUParamDiff < aDelta.first) && (aVParamDiff < aDelta.second))
						{
							toAppendNode = false;
							continue;
						}
					}
				}

				if (toAppendNode) 
				{
					aNodes->Append(gp_Pnt2d(aParam1, aParam2));
					aNodeMatrix.push_back({ aParam1, aParam2 });
				}
			}
		}
	}

	return aNodes;
}


//=======================================================================
// Function: getUndefinedIntervalNb
// Purpose : 
//=======================================================================
Standard_Integer BRepMesh_NURBSRangeSplitter::getUndefinedIntervalNb(
  const Handle(Adaptor3d_Surface)& theSurface,
  const Standard_Boolean           isU,
  const GeomAbs_Shape              /*theContinuity*/) const
{
  return (isU ? theSurface->NbUPoles() : theSurface->NbVPoles()) - 1;
}

//=======================================================================
// Function: getUndefinedInterval
// Purpose : 
//=======================================================================
void BRepMesh_NURBSRangeSplitter::getUndefinedInterval(
  const Handle(Adaptor3d_Surface)&               theSurface,
  const Standard_Boolean                         isU,
  const GeomAbs_Shape                            theContinuity,
  const std::pair<Standard_Real, Standard_Real>& theRange,
  TColStd_Array1OfReal&                          theIntervals) const
{
  Standard_Integer aIntervalsNb = isU ? 
    theSurface->NbUIntervals(theContinuity) :
    theSurface->NbVIntervals(theContinuity);

  if (aIntervalsNb == 1)
  {
    aIntervalsNb = getUndefinedIntervalNb(theSurface, isU, theContinuity);
    if (aIntervalsNb > 1)
    {
      theIntervals = TColStd_Array1OfReal(1, aIntervalsNb - 1);
      const Standard_Real aDiff = (theRange.second - theRange.first) / aIntervalsNb;
      for (Standard_Integer i = theIntervals.Lower(); i <= theIntervals.Upper(); ++i)
      {
        theIntervals.SetValue(i, theRange.first + i * aDiff);
      }
    }
  }

  if (theIntervals.IsEmpty())
  {
    theIntervals = TColStd_Array1OfReal(1, aIntervalsNb + 1);
    if (isU)
    {
      theSurface->UIntervals(theIntervals, theContinuity);
    }
    else
    {
      theSurface->VIntervals(theIntervals, theContinuity);
    }
  }
}

//=======================================================================
// Function: initParameters
// Purpose : 
//=======================================================================
Standard_Boolean BRepMesh_NURBSRangeSplitter::initParameters() const
{
  const GeomAbs_Shape aContinuity = GeomAbs_CN;
  const Handle(BRepAdaptor_Surface)& aSurface = GetSurface();

  TColStd_Array1OfReal aIntervals[2];
  getUndefinedInterval(aSurface, Standard_True,  aContinuity, GetRangeU(), aIntervals[0]);
  getUndefinedInterval(aSurface, Standard_False, aContinuity, GetRangeV(), aIntervals[1]);

  const Standard_Boolean isSplitIntervals = toSplitIntervals (aSurface->Surface().Surface(), aIntervals);

  if (!initParamsFromIntervals(aIntervals[0], GetRangeU(), isSplitIntervals,
	  const_cast<IMeshData::IMapOfReal&>(GetParametersU())))
  {
	  //if (!grabParamsOfEdges (Edge_Frontier, Param_U))
	  {
		  return Standard_False;
	  }
  }

  if (!initParamsFromIntervals(aIntervals[1], GetRangeV(), isSplitIntervals,
	  const_cast<IMeshData::IMapOfReal&>(GetParametersV())))
  {
	  //if (!grabParamsOfEdges (Edge_Frontier, Param_V))
	  {
		  return Standard_False;
	  }
  }

  return grabParamsOfEdges(Edge_Internal, Param_U | Param_V);
}


//=======================================================================
// Function: initParameters
// Purpose : 
//=======================================================================
Standard_Boolean BRepMesh_NURBSRangeSplitter::initNURBParameters() const
{
	const GeomAbs_Shape aContinuity = GeomAbs_CN;
	const Handle(BRepAdaptor_Surface)& aSurface = GetSurface();

	TColStd_Array1OfReal aIntervals[2];
	getUndefinedInterval(aSurface, Standard_True, aContinuity, GetRangeU(), aIntervals[0]);
	getUndefinedInterval(aSurface, Standard_False, aContinuity, GetRangeV(), aIntervals[1]);

	const Standard_Boolean isSplitIntervals = toSplitIntervals(aSurface->Surface().Surface(), aIntervals);

	if (!initParamsFromIntervals(aIntervals[0], GetRangeU(), isSplitIntervals,
		const_cast<IMeshData::IMapOfReal&>(GetParametersU())))
	{
		return Standard_False;
	}


	if (!initParamsFromIntervals(aIntervals[1], GetRangeV(), isSplitIntervals,
		const_cast<IMeshData::IMapOfReal&>(GetParametersV())))
	{
		return Standard_False;
	}

	return grabParamsOfEdges(Edge_Internal, Param_U | Param_V);
}

//=======================================================================
//function : grabParamsOfInternalEdges
//purpose  : 
//=======================================================================
Standard_Boolean BRepMesh_NURBSRangeSplitter::grabParamsOfEdges (
  const EdgeType         theEdgeType,
  const Standard_Integer theParamDimensionFlag) const
{
  if ((theParamDimensionFlag & (Param_U | Param_V)) == 0)
  {
    return Standard_False;
  }

  const IMeshData::IFaceHandle& aDFace = GetDFace ();
  for (Standard_Integer aWireIt = 0; aWireIt < aDFace->WiresNb (); ++aWireIt)
  {
    const IMeshData::IWireHandle& aDWire = aDFace->GetWire (aWireIt);
    for (Standard_Integer aEdgeIt = 0; aEdgeIt < aDWire->EdgesNb (); ++aEdgeIt)
    {
      const IMeshData::IEdgePtr& aDEdge = aDWire->GetEdge (aEdgeIt);
      for (Standard_Integer aPCurveIt = 0; aPCurveIt < aDEdge->PCurvesNb (); ++aPCurveIt)
      {
        const IMeshData::IPCurveHandle& aDPCurve = aDEdge->GetPCurve (aPCurveIt);
        if (aDPCurve->GetFace () == aDFace)
        {
          if (theEdgeType == Edge_Internal && !aDPCurve->IsInternal ())
          {
            continue;
          }

          for (Standard_Integer aPointIt = 0; aPointIt < aDPCurve->ParametersNb (); ++aPointIt)
          {
            const gp_Pnt2d& aPnt2d = aDPCurve->GetPoint (aPointIt);
            if (theParamDimensionFlag & Param_U)
            {
              const_cast<IMeshData::IMapOfReal&>(GetParametersU ()).Add (aPnt2d.X ());
            }

            if (theParamDimensionFlag & Param_V)
            {
              const_cast<IMeshData::IMapOfReal&>(GetParametersV ()).Add (aPnt2d.Y ());
            }
          }
        }
      }
    }
  }

  return Standard_True;
}

//=======================================================================
//function : computeGrainAndFilterParameters
//purpose  : 
//=======================================================================
Handle(IMeshData::SequenceOfReal) BRepMesh_NURBSRangeSplitter::computeGrainAndFilterParameters(
  const IMeshData::IMapOfReal&            theSourceParams,
  const Standard_Real                     theTol2d,
  const Standard_Real                     theRangeDiff,
  const Standard_Real                     theDelta,
  const IMeshTools_Parameters&            theParameters,
  const Handle(NCollection_IncAllocator)& theAllocator) const
{
  // Sort and filter sequence of parameters
  Standard_Real aMinDiff = Precision::PConfusion();
  if (theDelta < 1.)
  {
    aMinDiff /= theDelta;
  }

  const Handle(BRepAdaptor_Surface)& aSurface = GetSurface();
  const Standard_Real aMinSize2d = Max(
    aSurface->UResolution(theParameters.MinSize),
    aSurface->VResolution(theParameters.MinSize));

  aMinDiff = Max(aMinSize2d, aMinDiff);

  const Standard_Real aDiffMaxLim = 0.1 * theRangeDiff;
  const Standard_Real aDiffMinLim = Max(0.005 * theRangeDiff,
                                        2. * theTol2d);
  const Standard_Real aDiff = Max(aMinSize2d,
                                  Min(aDiffMaxLim, aDiffMinLim));
  return filterParameters(theSourceParams, aMinDiff*1e-1, aDiff*1e-1, theAllocator);
}

//=======================================================================
//function : filterParameters
//purpose  : 
//=======================================================================
Handle(IMeshData::SequenceOfReal) BRepMesh_NURBSRangeSplitter::filterParameters(
  const IMeshData::IMapOfReal&            theParams,
  const Standard_Real                     theMinDist,
  const Standard_Real                     theFilterDist,
  const Handle(NCollection_IncAllocator)& theAllocator) const
{
   Handle(IMeshData::SequenceOfReal) aResult = new IMeshData::SequenceOfReal(theAllocator);
  // Sort sequence of parameters
  const Standard_Integer anInitLen = theParams.Extent();

  if (anInitLen < 1)
  {
    return aResult;
  }

  TColStd_Array1OfReal aParamArray(1, anInitLen);

  for (Standard_Integer j = 1; j <= anInitLen; j++) 
  {
	  aParamArray(j) = theParams(j);
  }

  std::sort(aParamArray.begin(), aParamArray.end());

  //// Begin Print
  //std::cout << " " << std::endl;
  //std::cout << " Test FILTER " << std::endl;
  //for (j = 1; j <= anInitLen; j++) {
	 // std::cout << "Vertex " << j << ": " << aParamArray(j) << std::endl;
  //}
  //// End Print

  //std::cout << " " << std::endl;
  //std::cout << " aLastAdded " << std::endl;

  // mandatory pre-filtering using the first (minimal) filter value
  Standard_Integer aParamLength = 1;
  for (Standard_Integer j = 2; j <= anInitLen; j++)
  {
	  if ((aParamArray(j) - aParamArray(aParamLength)) > theMinDist)
	  {
		  if (++aParamLength < j)
		  {
			  aParamArray(aParamLength) = aParamArray(j);
		  }
	  }
  }

  //perform filtering on series
  Standard_Real aLastAdded, aLastCandidate;
  Standard_Boolean isCandidateDefined = Standard_False;
  aLastAdded = aParamArray(1);
  aLastCandidate = aLastAdded;
  aResult->Append(aLastAdded);

  /*Standard_Real count = 1;*/ //// Count used to print appended results

  for (Standard_Integer j = 2; j < aParamLength; j++)
  {
	  Standard_Real aVal = aParamArray(j);
	  if (aVal - aLastAdded > theFilterDist)
	  {
		  //adds the parameter
		  if (isCandidateDefined)
		  {
			  aLastAdded = aLastCandidate;
			  isCandidateDefined = Standard_False;
			  j--;
		  }
		  else
		  {
			  aLastAdded = aVal;
		  }
		  aResult->Append(aLastAdded);

		  //// Begin Print
		  //std::cout << "Vertex " << count << ": " << aLastAdded << std::endl;
		  //count++;
		  //// End Print

		  continue;
	  }

	  aLastCandidate = aVal;
	  isCandidateDefined = Standard_True;
  }
  aResult->Append(aParamArray(aParamLength));

  return aResult;
}
